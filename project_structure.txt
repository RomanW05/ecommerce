/myecommerce
|-- /microservice_api
|   |-- Dockerfile
|   |-- requirements.txt
|   |-- manage.py
|   |-- /ecommerce_store
|       |-- settings.py
|       |-- urls.py
|       |-- wsgi.py
|   |-- /static
|   |-- /media
|   |-- /templates
|	|-- /analytics
|		|-- /migrations
|			|-- __init__.py
|		|-- __init__.py
|		|-- admin.py
|		|-- apps.py
|		|-- models.py
|		|-- tests.py
|		|-- views.py
|	|-- user
|		|-- /migrations
|			|-- __init__.py
|		|-- __init__.py
|		|-- admin.py
|		|-- apps.py
|		|-- models.py
|		|-- tests.py
|		|-- views.py
|	|-- orders
|		|-- /migrations
|			|-- __init__.py
|		|-- __init__.py
|		|-- admin.py
|		|-- apps.py
|		|-- models.py
|		|-- tests.py
|		|-- views.py
|	|-- inventory
|		|-- /migrations
|			|-- __init__.py
|		|-- __init__.py
|		|-- admin.py
|		|-- apps.py
|		|-- models.py
|		|-- tests.py
|		|-- views.py
|   |-- marketing
|		|-- /migrations
|			|-- __init__.py
|		|-- __init__.py
|		|-- admin.py
|		|-- apps.py
|		|-- models.py
|		|-- tests.py
|		|-- views.py
|   |-- /tests
|       |-- /unit
|       |-- /integration
|       |-- /performance
|       |-- /coverage
|
|-- /microservice_users
|   |-- Dockerfile
|   |-- requirements.txt
|   |-- user_backend.py
|   |-- /tests
|       |-- /unit
|       |-- /integration
|       |-- /performance
|       |-- /coverage
|
|-- /microservice_orders
|   |-- Dockerfile
|   |-- requirements.txt
|   |-- orders_backend.py
|   |-- /tests
|       |-- /unit
|       |-- /integration
|       |-- /performance
|       |-- /coverage
|
|-- /microservice_inventory
|   |-- Dockerfile
|   |-- requirements.txt
|   |-- inventory_backend.py
|   |-- /tests
|       |-- /unit
|       |-- /integration
|       |-- /performance
|       |-- /coverage
|
|-- /microservice_analytics
|   |-- Dockerfile
|   |-- requirements.txt
|   |-- analytics_backend.py
|   |-- /tests
|       |-- /unit
|       |-- /integration
|       |-- /performance
|       |-- /coverage
|
|-- /microservice_marketing
|   |-- Dockerfile
|   |-- requirements.txt
|   |-- marketing_backend.py
|   |-- /tests
|       |-- /unit
|       |-- /integration
|       |-- /performance
|       |-- /coverage
|
|-- /kafka
|   |-- Dockerfile
|   |-- requirements.txt
|   |-- producer.py
|   |-- consumer.py
|
|-- /nginx
|   |-- Dockerfile
|   |-- default.conf
|
|-- /docker-compose.yml
|-- /azure
|   |-- azure-deployment.yml
|   |-- azure-services-setup.sh